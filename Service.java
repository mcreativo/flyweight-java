public class Service implements Flyweight
{
    protected String same;

    protected String different;

    public Service(String same)
    {
        this.same = same;
    }

    @Override
    public void setDifferent(String different)
    {
        this.different = different;
    }

    public void doSomething()
    {
        System.out.printf("doing something with attributes %s %s\n", same, different);
    }
}
