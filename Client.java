import java.util.UUID;
import java.util.stream.Stream;

public class Client
{
    public static void main(String[] args)
    {
        FlyweightFactory factory = new FlyweightFactory();

        Stream.generate(() -> {
            return factory.getService("aaaaaa");
        }).peek((s) -> s.setDifferent(UUID.randomUUID().toString())).limit(10).forEach(Service::doSomething);

        Stream.generate(() -> {
            return factory.getService("bbbbbb");
        }).peek((s) -> s.setDifferent(UUID.randomUUID().toString())).limit(10).forEach(Service::doSomething);
    }
}
