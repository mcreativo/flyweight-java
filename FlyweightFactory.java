import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FlyweightFactory
{
    protected Map<String, Service> flyweightList = new ConcurrentHashMap<>();

    public Service getService(String same)
    {
        if (!flyweightList.containsKey(same))
        {
            flyweightList.put(same, new Service(same));
        }

        return flyweightList.get(same);
    }
}
